package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;

import java.util.Date;

public class Battery extends SensorBase {

    private final Context context;
    private final BroadcastReceiver broadcastReceiver = new BatteryBroadcastReceiver();
    private static final IntentFilter batteryChangedBroadcastFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);

    public static final int VALUE_CHARGE = 0;
    public static final int VALUE_CHARGING = 1;
    public static final int VALUE_DISCHARGING = 2;
    public static final int VALUE_FULL = 3;

    public Battery(final Context context, final String name) {
        super(name);
        this.context = context;
        available = true;
        addField(VALUE_CHARGE, "Charge", "%");
        addField(VALUE_CHARGING, "Charging", "(bool)");
        addField(VALUE_DISCHARGING, "Discharging", "(bool)");
        addField(VALUE_FULL, "Full", "(bool)");
        setBytesRequiredForWholePart(1);
        setListening();
    }

    /* Hardware handling */
    private void startListening() {
        context.registerReceiver(broadcastReceiver, batteryChangedBroadcastFilter);
        notificateListeners();
    }

    private void stopListening() {
        try {
            context.unregisterReceiver(broadcastReceiver);
        } catch (Exception ignored) {
        }
        notificateListeners();
    }

    private void setListening() {
        if (used && !paused)
            startListening();
        else
            stopListening();
    }
    /* ... */

    @Override
    public void setUsed(boolean isUsed) {
        this.used = isUsed;
        setListening();
    }

    @Override
    public void setPaused(boolean isPaused) {
        this.paused = isPaused;
        setListening();
    }

    private class BatteryBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            currentValues = new float[4];

            // @source https://stackoverflow.com/a/15746919
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            currentValues[0] = (level / (float) scale) * 100;

            int batteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            currentValues[1] = (batteryStatus == BatteryManager.BATTERY_STATUS_CHARGING) ? 1 : 0;
            currentValues[2] = (batteryStatus == BatteryManager.BATTERY_STATUS_DISCHARGING) ? 1 : 0;
            currentValues[3] = (batteryStatus == BatteryManager.BATTERY_STATUS_FULL) ? 1 : 0;

            lastUpdate = new Date();
            notificateListeners();

        }
    }
}
