package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import hu.bme.igno1v.temalabor.virtuallorasensors.R;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorBase;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorList;

/**
 * Activity for changing used state of sensors.
 */
public class SensorSettingsActivity extends AppCompatActivity {

    /**
     * View group for sensors and their checkboxes.
     */
    ViewGroup layoutCheckboxesLinearLayout;

    /**
     * Inflate layout from R.layout.activity_sensor_settings, load sensors.
     *
     * @param savedInstanceState Not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        layoutCheckboxesLinearLayout = (ViewGroup) findViewById(R.id.layoutCheckboxesLinearLayout);
        Button saveAndGoBackButton = (Button) findViewById(R.id.saveAndGoBackButton);
        saveAndGoBackButton.setOnClickListener(new SaveButtonListener());

        loadSensors();

    }

    /**
     * Create a SensorSettingView for each view in the layoutCheckboxesLinearLayout group.
     */
    private void loadSensors() {
        for (SensorBase sensor : SensorList.getInstance().getSensorsList()) {
            View sensorSettingView = new SensorSettingView(this, sensor);
            layoutCheckboxesLinearLayout.addView(sensorSettingView);
        }
    }

    /**
     * Click listener of the save button.
     */
    private class SaveButtonListener implements View.OnClickListener {

        /**
         * When clicking save button, go back to previous activity.
         *
         * @param v View that is clicked
         */
        @Override
        public void onClick(View v) {
            onBackPressed();
        }

    }

    /**
     * Handle the click event of back button on the toolbar
     *
     * @param item Item that is selected on the toolbar
     * @return true if event is handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
