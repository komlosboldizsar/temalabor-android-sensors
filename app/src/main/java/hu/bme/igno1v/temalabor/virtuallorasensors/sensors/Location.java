package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

import java.util.Date;

public class Location extends SensorBase {

    private static final int LOCATION_REFRESH_TIME = 5000;
    private static final int LOCATION_REFRESH_DISTANCE = 10;

    private final LocationManager locationManager;
    private final String locationProvider;
    private final LocationListener locationListener = new MyLocationListener();

    public static final int VALUE_LAT = 0;
    public static final int VALUE_LON = 1;

    public Location(final LocationManager locationManager, final String locationProvider, final String name) {
        super(name);
        this.locationManager = locationManager;
        this.locationProvider = locationProvider;
        addField(VALUE_LAT, "Latitude", "°");
        addField(VALUE_LON, "Longitude", "°");
        setBytesRequiredForWholePart(1);
        setValuesCanBeSigned(true);
        setListening();
    }

    /* Hardware handling */
    private void startListening() {
        try {
            locationManager.requestLocationUpdates(locationProvider, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, locationListener);
            available = true;
        } catch (SecurityException e) {
            Log.e("LOCATION", "SecurityException: Locations are not available");
        }
        notificateListeners();
    }

    private void stopListening() {
        locationManager.removeUpdates(locationListener);
        notificateListeners();
    }

    private void setListening() {
        if (used && !paused)
            startListening();
        else
            stopListening();
    }
    /* ... */

    @Override
    public void setUsed(boolean isUsed) {
        this.used = isUsed;
        setListening();
    }

    @Override
    public void setPaused(boolean isPaused) {
        this.paused = isPaused;
        setListening();
    }

    private class MyLocationListener implements LocationListener {

        boolean providerAvailable = true;
        boolean providerEnabled = true;

        @Override
        public void onLocationChanged(android.location.Location location) {
            currentValues = new float[2];
            currentValues[0] = (float)location.getLatitude();
            currentValues[1] = (float)location.getLongitude();
            lastUpdate = new Date();
            notificateListeners();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            providerAvailable = (status == LocationProvider.AVAILABLE);
            refreshAvailableField();
            lastUpdate = new Date();
            notificateListeners();
        }

        @Override
        public void onProviderEnabled(String provider) {
            providerEnabled = true;
            refreshAvailableField();
        }

        @Override
        public void onProviderDisabled(String provider) {
            providerEnabled = false;
            refreshAvailableField();
        }

        private void refreshAvailableField() {
            available = providerAvailable && providerEnabled;
        }

    }

}
