package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.hardware.SensorManager;

public class LinearAcceleration extends PhysicalSensor {

    public static final int VALUE_X = 0;
    public static final int VALUE_Y = 1;
    public static final int VALUE_Z = 2;

    public LinearAcceleration(SensorManager sensorManager) {
        super(sensorManager, "Linear acceleration", android.hardware.Sensor.TYPE_LINEAR_ACCELERATION);
        addField(VALUE_X, "Axis X", "m/s²");
        addField(VALUE_Y, "Axis Y", "m/s²");
        addField(VALUE_Z, "Axis Z", "m/s²");
        setBytesRequiredForWholePart(1);
        setValuesCanBeSigned(true);
    }

}
