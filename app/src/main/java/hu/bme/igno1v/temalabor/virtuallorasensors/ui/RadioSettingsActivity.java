package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hu.bme.igno1v.temalabor.virtuallorasensors.R;
import hu.bme.igno1v.temalabor.virtuallorasensors.Settings;

/**
 * Activity for changing radio settings and transmission ra.te
 */
public class RadioSettingsActivity extends AppCompatActivity {

    /**
     * Input field for device ID.
     */
    private EditText deviceIDEditText;

    /**
     * Input field for the IP of the virtual LoRa radio socket.
     */
    private EditText socketIPEditText;

    /**
     * Input field for the port numbert of the virtual LoRa radio socket.
     */
    private EditText socketNumberEditText;

    /**
     * Drop-down for setting rate of transmission.
     */
    private Spinner transmissionRateSpinner;

    /**
     * Save settings button.
     */
    private Button saveSettingsButton;

    /**
     * Cancel button.
     */
    private Button cancelSettingsButton;

    /**
     * Choosable values of the transmission rate drop-down.
     */
    private List<TransmissionRate> transmissionRateValues;

    /**
     * Initialize the view, load layout from R.layout.activity_radio_settings and bind fields.
     *
     * @param savedInstanceState Not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_settings);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        deviceIDEditText = (EditText) findViewById(R.id.deviceID);
        deviceIDEditText.addTextChangedListener(new DeviceIDEditTextChangeListener());

        socketIPEditText = (EditText) findViewById(R.id.socketIP);
        socketIPEditText.addTextChangedListener(new SocketIPEditTextChangeListener());

        socketNumberEditText = (EditText) findViewById(R.id.socketPort);
        socketNumberEditText.addTextChangedListener(new SocketNumberEditTextChangeListener());

        View.OnClickListener buttonClickListener = new ButtonClickListener();

        saveSettingsButton = (Button) findViewById(R.id.saveSettingsButton);
        saveSettingsButton.setOnClickListener(buttonClickListener);

        cancelSettingsButton = (Button) findViewById(R.id.cancelSettingsButton);
        cancelSettingsButton.setOnClickListener(buttonClickListener);

        transmissionRateSpinner = (Spinner) findViewById(R.id.transmissionRate);
        populateTransmissionRateSpinner();
        transmissionRateSpinner.setOnItemSelectedListener(new TransmissionRateSpinnerSelectedListener());

        loadSettings();

    }

    /**
     * Fill the transmissionRateValues list with possible values.
     */
    private void populateTransmissionRateSpinner() {

        transmissionRateValues = new ArrayList<>();
        transmissionRateValues.add(new TransmissionRate("0.5 s", 500));
        transmissionRateValues.add(new TransmissionRate("1 s", 1000));
        transmissionRateValues.add(new TransmissionRate("2 s", 2000));
        transmissionRateValues.add(new TransmissionRate("5 s", 5000));
        transmissionRateValues.add(new TransmissionRate("10 s", 10 * 1000));
        transmissionRateValues.add(new TransmissionRate("30 s", 30 * 1000));
        transmissionRateValues.add(new TransmissionRate("1 min", 60 * 1000));
        transmissionRateValues.add(new TransmissionRate("2 min", 2 * 60 * 1000));
        transmissionRateValues.add(new TransmissionRate("5 min", 5 * 60 * 1000));
        transmissionRateValues.add(new TransmissionRate("10 min", 10 * 60 * 1000));
        transmissionRateValues.add(new TransmissionRate("15 min", 15 * 60 * 1000));
        transmissionRateValues.add(new TransmissionRate("30 min", 30 * 60 * 1000));
        transmissionRateValues.add(new TransmissionRate("1 hour", 60 * 60 * 1000));

        ArrayAdapter<TransmissionRate> transmissionRateSpinnerAdapter = new ArrayAdapter<TransmissionRate>(this, android.R.layout.simple_spinner_dropdown_item, transmissionRateValues);
        transmissionRateSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        transmissionRateSpinner.setAdapter(transmissionRateSpinnerAdapter);

    }

    /**
     * Load settings from Settings class and fill the inputs.
     */
    private void loadSettings() {

        String deviceID = Settings.getDeviceID();
        String socketIP = Settings.getLoraSocketIP();
        int socketPort = Settings.getLoraSocketPort();
        int transmissionRate = Settings.getTransmissionRate();

        deviceIDEditText.setText(deviceID);
        socketIPEditText.setText(socketIP);
        socketNumberEditText.setText(Integer.toString(socketPort));

        int transmissionRateSpinnerOptionIndex = -1;
        int i = 0;
        for (TransmissionRate tr : transmissionRateValues) {
            if (tr.milliseconds == transmissionRate) {
                transmissionRateSpinnerOptionIndex = i;
                break;
            }
            i++;
        }
        if (transmissionRateSpinnerOptionIndex > -1)
            transmissionRateSpinner.setSelection(transmissionRateSpinnerOptionIndex);
        else
            transmissionRateSpinner.setSelection(0);

    }

    /**
     * Save inputs to the Settings class.
     */
    private void saveSettings() {

        String deviceID = deviceIDEditText.getText().toString();
        Settings.setDeviceID(deviceID);

        String socketIP = socketIPEditText.getText().toString();
        Settings.setLoraSocketIP(socketIP);

        int socketPort = Integer.parseInt(socketNumberEditText.getText().toString());
        Settings.setLoraSocketPort(socketPort);

        TransmissionRate selectedTransmissionRate = (TransmissionRate) transmissionRateSpinner.getSelectedItem();
        Settings.setTransmissionRate(selectedTransmissionRate.milliseconds);

        Toast.makeText(this, "Settings changed.", Toast.LENGTH_SHORT).show();

    }


    //region Validation

    /**
     * True if the device ID in the deviceIDEditText is valid.
     */
    private boolean deviceIDValid = true;

    /**
     * True if the IP in the socketIPEdiText is valid.
     */
    private boolean socketIPValid = true;

    /**
     * True if the port in socketPortEditText is valid.
     */
    private boolean socketPortValid = true;

    /**
     * True if the chosen transmission rate is valid.
     */
    private boolean transissionRateSpinnerValid = true;

    /**
     * Enable or disable the save button by the validation state of fields.
     */
    private void fieldValidated() {
        boolean valid =
                deviceIDValid
                        && socketIPValid
                        && socketPortValid
                        && transissionRateSpinnerValid;
        saveSettingsButton.setEnabled(valid);
    }

    /**
     * Expected format of device ID.
     */
    private static final String DEVICE_ID_REGEXP = "^([0-9a-fA-F]{2}:){7}([0-9a-fA-F]{2})$";

    /**
     * Validate the deviceIDEditText input.
     */
    private void validateDeviceIDEditText() {
        if (!deviceIDEditText.getText().toString().matches(DEVICE_ID_REGEXP)) {
            deviceIDEditText.setError("Excepted format is 8 bytes in hexadecimal format, separated by colon.");
            deviceIDValid = false;
        } else {
            socketIPEditText.setError(null);
            deviceIDValid = true;
        }
        fieldValidated();
    }

    /**
     * Validate the socketIPEditText input.
     */
    private void validateSocketIPEditText() {
        if (socketIPEditText.getText().toString().isEmpty()) {
            socketIPEditText.setError("This field can't be empty!");
            socketIPValid = false;
        } else {
            socketIPEditText.setError(null);
            socketIPValid = true;
        }
        fieldValidated();
    }

    /**
     * Validate the socketNumberEditText input.
     */
    private void validateSocketPortEditText() {

        if (socketNumberEditText.getText().toString().isEmpty()) {
            socketNumberEditText.setError("This field can't be empty!");
            socketPortValid = false;
        } else {
            socketNumberEditText.setError(null);
            socketPortValid = true;
        }

        fieldValidated();
    }

    /**
     * Validate the transmissionRateSpinner dropdown.
     */
    private void validateTransmissionRateSpinner() {
        transissionRateSpinnerValid = (transmissionRateSpinner.getSelectedItemPosition() > -1);
        fieldValidated();
    }
    //endregion

    /* ... */

    /**
     * Handle the click event of back button on the toolbar
     *
     * @param item Item that is selected on the toolbar
     * @return true if event is handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Structure for storing options for the transmissionRateSpinner.
     */
    private class TransmissionRate {

        /**
         * Label to show in the drop-down.
         */
        public final String label;

        /**
         * Meaning in milliseconds.
         */
        public final int milliseconds;

        /**
         * Initialize the structure.
         *
         * @param label        Label to show in the drop-down
         * @param milliseconds Meaning in milliseconds
         */
        public TransmissionRate(final String label, int milliseconds) {
            this.label = label;
            this.milliseconds = milliseconds;
        }

        /**
         * Get string value of the option.
         *
         * @return Value of the label field
         */
        public String toString() {
            return label;
        }

    }

    /**
     * Listener of item select event of transmissionRateSpinner.
     */
    private class TransmissionRateSpinnerSelectedListener implements AdapterView.OnItemSelectedListener {

        /**
         * Called when an item is selected.
         *
         * @param parent   View where the item is selected
         * @param view     The view of the selected item
         * @param position The position of the selected item
         * @param id       ?
         */
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            validateTransmissionRateSpinner();
        }

        /**
         * Called when no item is selected.
         *
         * @param parent View where the selection change happened
         */
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            validateTransmissionRateSpinner();
        }

    }

    /**
     * Listener of buttons in the activity.
     */
    private class ButtonClickListener implements View.OnClickListener {

        /**
         * Handle click event of buttons.
         *
         * @param v Button that is clicked
         */
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.saveSettingsButton:
                    saveSettings();
                    onBackPressed();
                    break;
                case R.id.cancelSettingsButton:
                    onBackPressed();
                    break;
            }
        }

    }

    /**
     * Empty text watcher class. All TextWatcher interface methods are implemented
     * with empty bodies, so extending classes must not implement all of them.
     */
    private class EmptyTextWatcher implements TextWatcher {

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * are about to be replaced by new text with length <code>after</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param count
         * @param after
         */
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        /**
         * This method is called to notify you that, within <code>s</code>,
         * the <code>count</code> characters beginning at <code>start</code>
         * have just replaced old text that had length <code>before</code>.
         * It is an error to attempt to make changes to <code>s</code> from
         * this callback.
         *
         * @param s
         * @param start
         * @param before
         * @param count
         */
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        /**
         * This method is called to notify you that, somewhere within
         * <code>s</code>, the text has been changed.
         *
         * @param s
         */
        @Override
        public void afterTextChanged(Editable s) {
        }

    }

    /**
     * Class for listning text change of deviceIDEditText.
     */
    private class DeviceIDEditTextChangeListener extends EmptyTextWatcher {
        /**
         * Validate field if changed
         *
         * @param s Which field is changed (hopefully deviceIDEditText)
         */
        @Override
        public void afterTextChanged(Editable s) {
            validateDeviceIDEditText();
        }
    }

    /**
     * Class for listning text change of socketIPEditText.
     */
    private class SocketIPEditTextChangeListener extends EmptyTextWatcher {
        /**
         * Validate field if changed
         *
         * @param s Which field is changed (hopefully deviceIDEditText)
         */
        @Override
        public void afterTextChanged(Editable s) {
            validateSocketIPEditText();
        }
    }

    /**
     * Class for listning text change of socketPortEditText.
     */
    private class SocketNumberEditTextChangeListener extends EmptyTextWatcher {
        /**
         * Validate field if changed
         *
         * @param s Which field is changed (hopefully deviceIDEditText)
         */
        @Override
        public void afterTextChanged(Editable s) {
            validateSocketPortEditText();
        }
    }

}
