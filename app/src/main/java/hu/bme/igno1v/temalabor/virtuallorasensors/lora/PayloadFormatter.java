package hu.bme.igno1v.temalabor.virtuallorasensors.lora;

import java.util.LinkedList;
import java.util.List;

import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.PhysicalSensor;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorBase;

public class PayloadFormatter {

    private List<PayloadPart> parts = new LinkedList<>();
    private int sumLength = 0;

    public void addPart(SensorBase sensor, int valueIndex, int length) {
        PayloadPart part = new PayloadPart(sensor, valueIndex, length);
        parts.add(part);
        sumLength += length;
    }

    public void clearParts() {
        parts.clear();
        sumLength = 0;
    }

    public byte[] getPayload() {

        byte[] payload = new byte[sumLength];
        int position = 0;

        for (PayloadPart part : parts) {
            byte[] partBytes = part.sensor.getBytes(part.valueIndex, part.length);
            for (int i = 0; i < part.length; i++)
                payload[position++] = partBytes[i];
        }

        return payload;

    }

    public List<PayloadPart> getParts() {
        return parts;
    }

    public class PayloadPart {
        public final SensorBase sensor;
        public final int valueIndex;
        public final int length;

        public PayloadPart(final SensorBase sensor, int valueIndex, int length) {
            this.sensor = sensor;
            this.valueIndex = valueIndex;
            this.length = length;
        }
    }

}
