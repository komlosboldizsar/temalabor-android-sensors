package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Pager adapter for the ViewPager of the MainActivity.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    /**
     * Create the pager adapter.
     *
     * @param fm FragmentManager that manages the page fragments
     */
    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    /**
     * Get fragment for a page.
     *
     * @param position Page index (starting with 0) to get the fragment for
     * @return Fragment to show on the page
     */
    @Override
    public Fragment getItem(int position) {
        Fragment ret = null;
        switch (position) {
            case 0:
                ret = new MainValuesFragment();
                break;
            case 1:
                ret = new MainLogFragment();
                break;
        }
        return ret;
    }

    /**
     * Get the title of the page with given index.
     *
     * @param position Which page's title to get (indexing starting with 0)
     * @return Title of the page with given index
     */
    @Override
    public CharSequence getPageTitle(int position) {
        String title;
        switch (position) {
            case 0:
                title = "Measured values";
                break;
            case 1:
                title = "Log";
                break;
            default:
                title = "";
        }
        return title;
    }

    /**
     * Get count of pages.
     *
     * @return 2
     */
    @Override
    public int getCount() {
        return 2;
    }

}