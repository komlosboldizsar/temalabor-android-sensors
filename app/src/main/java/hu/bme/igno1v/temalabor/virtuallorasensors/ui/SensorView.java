package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import hu.bme.igno1v.temalabor.virtuallorasensors.R;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.PhysicalSensor;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorBase;

/**
 * View to show the data, state and values of a sensor.
 */
public class SensorView extends LinearLayout implements SensorBase.SensorListener {

    /**
     * Sensor to show in the view.
     */
    SensorBase sensor;

    /**
     * Context where the view is shown.
     */
    Context context;

    /**
     * Inflater used to inflate the view from XML.
     */
    LayoutInflater inflater;

    /**
     * View group that contains fields for the sensor.
     */
    ViewGroup fieldsList;

    /**
     * Label that shows the name of the sensor.
     */
    TextView sensorNameTextView;

    /**
     * Label that shows the time of the last data update from sensor.
     */
    TextView sensorLastUpdateTextView;

    /**
     * Labels that show the name of fields of the sensor.
     */
    TextView[] fieldNameTextViews;

    /**
     * Label that show the value of the fields of the sensor.
     */
    TextView[] fieldValueTextViews;

    /**
     * Label that show the unit of values of the fields of the sensor.
     */
    TextView[] fieldUnitTextViews;

    /**
     * Color to use for displaying value when sensor is not available.
     */
    int COLOR_SENSOR_NOT_AVAILABLE = Color.argb(100, 102, 102, 153);

    /**
     * Color to use for displaying value when sensor is not used.
     */
    int COLOR_SENSOR_NOT_USED = Color.argb(100, 163, 194, 194);

    /**
     * Color to use for displaying value when sensor listening is paused.
     */
    int COLOR_SENSOR_PAUSED = Color.argb(100, 100, 100, 255);

    /**
     * Color to use for displaying value when no data from sensor is available.
     */
    int COLOR_SENSOR_NO_DATA = Color.argb(100, 255, 102, 153);

    /**
     * Color to use for displaying value when there is displayable data.
     */
    int COLOR_SENSOR_DATA_OK = Color.argb(100, 0, 153, 0);

    /**
     * Date format used to show time of last data refresh.
     */
    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss.S");

    /**
     * Create view, fill labels, create TextViews for fields based on the data from sensor.
     * Subscribe to data update events of the sensor.
     *
     * @param context Context where view is displayed
     * @param sensor  Sensor that's data is shown by this view
     */
    public SensorView(Context context, SensorBase sensor) {

        super(context);
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutInflater.from(context).inflate(R.layout.sensorview, this);

        this.sensor = sensor;
        sensor.registerListener(this);

        createValueFields();
        getAndDisplayValues();

    }

    /**
     * When view is detached from window (not shown anymore), unsubscribe from sensor events.
     */
    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        sensor.unregisterListener(this);
    }

    /**
     * Create TextViews for all fields of the sensor.
     */
    private void createValueFields() {

        fieldsList = (ViewGroup) findViewById(R.id.sensorFields);

        sensorNameTextView = (TextView) findViewById(R.id.sensorName);
        sensorNameTextView.setText(sensor.getName());

        sensorLastUpdateTextView = (TextView) findViewById(R.id.sensorLastUpdate);
        sensorLastUpdateTextView.setText("?");

        int fieldCount = sensor.getFieldCount();
        fieldNameTextViews = new TextView[fieldCount];
        fieldValueTextViews = new TextView[fieldCount];
        fieldUnitTextViews = new TextView[fieldCount];

        for (int i = 0; i < fieldCount; i++) {
            View fieldView = inflater.inflate(R.layout.sensorfield, null);
            fieldsList.addView(fieldView);
            fieldNameTextViews[i] = (TextView) fieldView.findViewById(R.id.fieldName);
            fieldValueTextViews[i] = (TextView) fieldView.findViewById(R.id.fieldValue);
            fieldUnitTextViews[i] = (TextView) fieldView.findViewById(R.id.fieldUnit);
        }

    }

    /**
     * Get values of fields from sensor and display them.
     */
    private void getAndDisplayValues() {

        Map<String, PhysicalSensor.SensorValue> fieldValues = sensor.getValues();

        int i = 0;
        for (Map.Entry<String, PhysicalSensor.SensorValue> fieldValue : fieldValues.entrySet()) {

            fieldNameTextViews[i].setText(fieldValue.getKey());

            String valueText = "";
            String unitText = "";
            int fieldColor = Color.BLACK;
            PhysicalSensor.SensorValue valuePack = fieldValue.getValue();
            switch (valuePack.flag) {
                case SENSOR_NOT_AVAILABLE:
                    valueText = "not available";
                    fieldColor = COLOR_SENSOR_NOT_AVAILABLE;
                    break;
                case SENSOR_NOT_USED:
                    valueText = "not used";
                    fieldColor = COLOR_SENSOR_NOT_USED;
                    break;
                case SENSOR_PAUSED:
                    valueText = "listening paused";
                    fieldColor = COLOR_SENSOR_PAUSED;
                    break;
                case SENSOR_NO_DATA:
                    valueText = "no data";
                    fieldColor = COLOR_SENSOR_NO_DATA;
                    break;
                case SENSOR_DATA_OK:
                    valueText = Float.toString(valuePack.value);
                    unitText = valuePack.unit;
                    fieldColor = COLOR_SENSOR_DATA_OK;
                    break;
            }

            TextView fieldValueTextView = fieldValueTextViews[i];
            fieldValueTextView.setText(valueText);
            fieldValueTextView.setTextColor(fieldColor);

            TextView fieldUnitTextView = fieldUnitTextViews[i];
            fieldUnitTextView.setText(unitText);
            fieldUnitTextView.setTextColor(fieldColor);

            i++;

        }

    }

    /**
     * Notify the view that data of an observed sensor is changed.
     *
     * @param sensor Sensor thats data is changed
     */
    @Override
    public void sensorDataChanged(SensorBase sensor) {
        getAndDisplayValues();
        Date lastUpd = sensor.getLastUpdate();
        String lastUpdStr = "?";
        if (lastUpd != null)
            lastUpdStr = DATE_FORMAT.format(lastUpd);
        sensorLastUpdateTextView.setText(lastUpdStr);
    }

}
