package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import hu.bme.igno1v.temalabor.virtuallorasensors.R;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorBase;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorList;

/**
 * Fragment for showing values of sensors on the MainActivity.
 */
public class MainValuesFragment extends android.support.v4.app.Fragment implements SensorList.SensorListChangeListener {

    /**
     * Container activity.
     */
    private Activity mainActivity;

    /**
     * ViewGroup that contains the SensorView elements.
     */
    private ViewGroup contentLayoutGroup;

    /**
     * Create the fragment.
     *
     * @param savedInstanceState Not used
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = getActivity();
    }

    /**
     * Create the view of the fragment by R.layout.fragment_main_values layout.
     *
     * @param inflater           Inflater used to inflate the layout XML
     * @param container          Root/parent/container view of the created view
     * @param savedInstanceState Not used
     * @return The view inflated by R.layout.fragment_main_values
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_main_values, container, false);
        contentLayoutGroup = (LinearLayout) layout.findViewById(R.id.content);
        SensorList.getInstance().registerSensorListChangeListener(this);
        reloadSensorList();
        return layout;
    }

    /**
     * If sensor list changed, reload it.
     */
    @Override
    public void sensorListChanged() {
        reloadSensorList();
    }

    /**
     * Reload list of sensors: remove all SensorView elements from contentLayoutGroup
     * and create them again by the new list.
     */
    private void reloadSensorList() {
        contentLayoutGroup.removeAllViews();
        for (SensorBase s : SensorList.getInstance().getSensorsList())
            addSensor(s);
    }

    /**
     * Add a new sensor: create a SensorView instance and add it to the contentLayoutGroup list.
     *
     * @param sensor Sensor to add
     */
    private void addSensor(SensorBase sensor) {
        SensorView sensorView = new SensorView(mainActivity, sensor);
        contentLayoutGroup.addView(sensorView);
        sensor.setUsed(true);
    }

}
