package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.hardware.SensorManager;

public class Gyroscope extends PhysicalSensor {

    public final int VALUE_X = 0;
    public final int VALUE_Y = 1;
    public final int VALUE_Z = 2;

    public Gyroscope(SensorManager sensorManager) {
        super(sensorManager, "Gyroscope", android.hardware.Sensor.TYPE_GYROSCOPE);
        addField(VALUE_X, "Axis X", "rad/s");
        addField(VALUE_Y, "Axis Y", "rad/s");
        addField(VALUE_Z, "Axis Z", "rad/s");
        setBytesRequiredForWholePart(2);
        setValuesCanBeSigned(true);
    }

}
