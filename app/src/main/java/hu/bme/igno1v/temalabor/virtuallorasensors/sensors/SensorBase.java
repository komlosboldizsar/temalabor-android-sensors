package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class SensorBase {

    public final String name;
    private int bytesRequiredForWholePart;
    private boolean valuesCanBeSigned = false;

    private final Map<Integer, Field> fields = new TreeMap<>();
    protected float[] currentValues = null;
    protected Date lastUpdate;

    protected boolean paused = false;
    protected boolean used = false;
    protected boolean available;

    private final List<SensorListener> listeners = new LinkedList<>();

    public SensorBase(final String name) {
        this.name = name;
    }

    public boolean isAvailable() {
        return available;
    }

    /* Hardware handling */
    protected void addField(int valuePosition, final String name) {
        fields.put(valuePosition, new Field(name, ""));
    }

    protected void addField(int valuePosition, final String name, final String unit) {
        fields.put(valuePosition, new Field(name, unit));
    }

    public int getFieldCount() {
        return fields.size();
    }

    public String getFieldName(int fieldIndex) {
        return fields.get(fieldIndex).name;
    }

    public String getName() {
        return name;
    }

    public Map<String, SensorValue> getValues() {

        Map<String, SensorValue> values = new TreeMap<>();

        for (Map.Entry<Integer, Field> field : fields.entrySet()) {

            String key = field.getValue().name;
            SensorValue value;

            if (!available)
                value = new SensorValue(ValueFlag.SENSOR_NOT_AVAILABLE);
            else if (!used)
                value = new SensorValue(ValueFlag.SENSOR_NOT_USED);
            else if (paused)
                value = new SensorValue(ValueFlag.SENSOR_PAUSED);
            else if ((currentValues != null) && (currentValues.length > field.getKey()))
                value = new SensorValue(currentValues[field.getKey()], field.getValue().unit);
            else
                value = new SensorValue(ValueFlag.SENSOR_NO_DATA);

            values.put(key, value);
        }

        return values;

    }

    public void setUsed(boolean isUsed) {
        this.used = isUsed;
    }

    public boolean isUsed() {
        return used;
    }

    public void setPaused(boolean isPaused) {
        this.paused = isPaused;
    }

    public void registerListener(SensorListener listener) {
        listeners.add(listener);
    }

    public void unregisterListener(SensorListener listener) {
        listeners.remove(listener);
    }

    protected void notificateListeners() {
        for (SensorListener listener : listeners)
            listener.sensorDataChanged(this);
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    protected void setBytesRequiredForWholePart(int numOfBytes) {
        this.bytesRequiredForWholePart = numOfBytes;
    }

    protected void setValuesCanBeSigned(boolean canBeSigned) {
        this.valuesCanBeSigned = canBeSigned;
    }

    public byte[] getBytes(int valueIndex, int numOfBytes) {

        byte[] returnBytes = new byte[numOfBytes];
        int b = 0;

        if ((currentValues != null) && (valueIndex < currentValues.length)) {

            if (valuesCanBeSigned) {
                if (currentValues[valueIndex] < 0)
                    returnBytes[b++] = (byte) 0xFF;
                else
                    returnBytes[b++] = 0x00;
            }
            float value = Math.abs(currentValues[valueIndex]);

            int startOffset = bytesRequiredForWholePart - 1;
            int endOffset = startOffset - (numOfBytes - b);

            int t = (int) value;
            for (int i = startOffset; i > endOffset; i--) {

                if (i >= 0) {
                    returnBytes[b++] = (byte) (t >> (8 * i));
                } else {
                    value *= 256;
                    returnBytes[b++] = (byte) value;
                }

            }

        }

        return returnBytes;

    }

    public int getBytesForWholePart() {
        if (!valuesCanBeSigned)
            return bytesRequiredForWholePart;
        return bytesRequiredForWholePart + 1;
    }

    public class SensorValue {
        public final ValueFlag flag;
        public final float value;
        public final String unit;

        public SensorValue(ValueFlag type) {
            this.flag = type;
            this.value = 0;
            this.unit = "";
        }

        public SensorValue(float value, final String unit) {
            this.flag = ValueFlag.SENSOR_DATA_OK;
            this.value = value;
            this.unit = unit;
        }
    }

    public enum ValueFlag {
        SENSOR_NOT_AVAILABLE,
        SENSOR_NOT_USED,
        SENSOR_PAUSED,
        SENSOR_NO_DATA,
        SENSOR_DATA_OK
    }

    public interface SensorListener {
        void sensorDataChanged(SensorBase sensor);
    }

    public class Field {
        public final String name;
        public final String unit;

        public Field(final String name, final String unit) {
            this.name = name;
            this.unit = unit;
        }
    }

}
