package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.hardware.SensorManager;

public class RotationVector extends PhysicalSensor {

    public final int VALUE_X = 0;
    public final int VALUE_Y = 1;
    public final int VALUE_Z = 2;

    public RotationVector(SensorManager sensorManager) {
        super(sensorManager, "Rotation vector", android.hardware.Sensor.TYPE_ROTATION_VECTOR);
        addField(VALUE_X, "Axis X");
        addField(VALUE_Y, "Axis Y");
        addField(VALUE_Z, "Axis Z");
        setBytesRequiredForWholePart(2);
        setValuesCanBeSigned(true);
    }

}
