package hu.bme.igno1v.temalabor.virtuallorasensors.lora;

import android.os.AsyncTask;
import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

import hu.bme.igno1v.temalabor.virtuallorasensors.Logger;

/**
 * @source https://stackoverflow.com/questions/6531950/how-to-execute-async-task-repeatedly-after-fixed-time-intervals
 */
public class TransmitScheduler {

    /* Singleton */

    private static TransmitScheduler instance = null;

    private TransmitScheduler() {
        // empty
    }

    public static TransmitScheduler getInstance() {
        if (instance == null)
            instance = new TransmitScheduler();
        return instance;
    }

    /* ... */

    private final Handler handler = new Handler();
    private Timer timer = new Timer();
    private TimerTask transmitTimerTask = new TransmitTimerTask();

    private int transmitRate = 500;
    private boolean transmitting = false;

    private PayloadFormatter payloadFormatter;

    public void setTransmitting(boolean transmitting) {
        this.transmitting = transmitting;
        updateTimer();
    }

    public void setTransmitRate(int transmitRate) {
        this.transmitRate = transmitRate;
        updateTimer();
    }

    private void updateTimer() {

        try {
            timer.cancel();
            transmitTimerTask.cancel();
        } catch (IllegalStateException e) {
            // Do nothing...
        }

        timer = new Timer();
        transmitTimerTask = new TransmitTimerTask();

        if (transmitting)
            timer.schedule(transmitTimerTask, 0, transmitRate);

    }

    public void setPayloadFormatter(PayloadFormatter payloadFormatter) {
        this.payloadFormatter = payloadFormatter;
    }

    public PayloadFormatter getPayloadFormatter() {
        return payloadFormatter;
    }

    private class TransmitTimerTask extends TimerTask {

        @Override
        public void run() {
            handler.post(new Runnable() {
                public void run() {
                    try {
                        TransmitTask transmitTask = new TransmitTask();
                        transmitTask.execute();
                    } catch (Exception e) {
                        // ...
                    }
                }
            });
        }

    }

    private void doTransmission() {
        if(payloadFormatter != null) {
            byte[] payload = payloadFormatter.getPayload();
            RadioInterface.getInstance().transmit(payload);
        }
    }

    private class TransmitTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            Logger.log("Starting scheduled transmission...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            doTransmission();
            return null;
        }

    }

}
