package hu.bme.igno1v.temalabor.virtuallorasensors.lora;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;

import hu.bme.igno1v.temalabor.virtuallorasensors.Logger;

public class RadioInterface {

    /* Singleton */

    private static RadioInterface instance = null;

    private RadioInterface() {

    }

    public static RadioInterface getInstance() {
        if (instance == null)
            instance = new RadioInterface();
        return instance;
    }

    /* ... */

    public void transmit(final byte[] frmpayload) {
        Logger.log("Transmitting... FRM payload: " + bytesToHex(frmpayload), Logger.MessageType.LOG_NEUTRAL);
        byte[] phypayload = getPhyPayload(frmpayload);
        send(phypayload);
    }

    private int frameCount = 0;

    private void send(final byte[] phypayload) {

        try {
            DatagramSocket s = new DatagramSocket();
            InetAddress local = InetAddress.getByName(socketIP);
            DatagramPacket p = new DatagramPacket(phypayload, phypayload.length, local, socketPortNumber);
            s.send(p);
            Logger.log("Transmission complete.", Logger.MessageType.LOG_SUCCESS);
            frameCount++;
        } catch (SocketException e) {
            Logger.log("[SocketException] " + e.getMessage(), Logger.MessageType.LOG_ERROR);
        } catch (UnknownHostException e) {
            Logger.log("[UnknownHostException] " + e.getMessage(), Logger.MessageType.LOG_ERROR);
        } catch (IOException e) {
            Logger.log("[IOException] " + e.getMessage(), Logger.MessageType.LOG_ERROR);
        }

    }

    private byte[] getPhyPayload(byte[] frmpayload) {

        byte[] mhdr = getMhdr();
        byte[] macpayload = getMacPayload(frmpayload);
        byte[] mic = getMic();

        return concatArrays(mhdr, macpayload, mic);

    }

    private final byte MTYPE_UNCONFIRMED_DATA_UP = 0b010;
    private final byte HEADER_MHDR_MTYPE = MTYPE_UNCONFIRMED_DATA_UP;

    private byte[] getMhdr() {
        byte major = 0;
        return new byte[]{
                (Byte) (byte) (((HEADER_MHDR_MTYPE & 0b00000111) << 5)
                        | ((major & 0b00000011)))
        };
    }

    private byte[] getMic() {
        return new byte[]{0, 0, 0, 0}; // TODO
    }

    private final byte HEADER_FPORT = 0x01;

    private byte[] getMacPayload(byte[] frmpayload) {

        byte[] fhdr = getFhdr();
        byte[] fport = new byte[]{HEADER_FPORT};

        return concatArrays(fhdr, fport, frmpayload);

    }

    private final byte HEADER_ADR = 0; // 1 bit
    private final byte HEADER_ADRACKREQ = 0; // 1 bit
    private final byte HEADER_ACK = 0; // 1 bit
    private final byte HEADER_RFU = 0; // 1 bit

    private byte[] getFhdr() {

        byte[] fopts = new byte[]{};

        int b = ((HEADER_ADR & 0b00000001) << 7)
                | ((HEADER_ADRACKREQ & 0b00000001) << 6)
                | ((HEADER_ACK & 0b00000001) << 5)
                | ((HEADER_RFU & 0b00000001) << 4)
                | (fopts.length & 0b00000111);
        byte[] fctrl = new byte[]{
                (byte) b
        };

        byte fcntMSB = (byte) ((frameCount >> 8) & 0xFF);
        byte fcntLSB = (byte) (frameCount & 0xFF);
        byte[] fcnt = new byte[]{fcntLSB, fcntMSB};

        byte[] devid = new byte[]{
                deviceID[3],
                deviceID[2],
                deviceID[1],
                deviceID[0]
        };

        return concatArrays(devid, fctrl, fcnt, fopts);

    }

    private byte[] concatArrays(byte[]... arrays) {

        int sumLength = 0;
        for (byte[] array : arrays)
            sumLength += array.length;

        byte[] concatArray = new byte[sumLength];
        int b = 0;
        for (byte[] array : arrays)
            for (byte element : array)
                concatArray[b++] = element;

        return concatArray;

    }

    /* Attributes, setters and getters */

    private byte[] deviceID;
    private String socketIP;
    private int socketPortNumber;

    public byte[] getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(byte[] deviceID) {
        this.deviceID = deviceID;
    }

    public String getSocketIP() {
        return socketIP;
    }

    public void setSocketIP(String socketIP) {
        this.socketIP = socketIP;
    }

    public int getSocketPortNumber() {
        return socketPortNumber;
    }

    public void setSocketPortNumber(int socketPortNumber) {
        this.socketPortNumber = socketPortNumber;
    }

    /* Helpers */

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 3];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
            hexChars[j * 3 + 2] = ' ';
        }
        return new String(hexChars);
    }


}
