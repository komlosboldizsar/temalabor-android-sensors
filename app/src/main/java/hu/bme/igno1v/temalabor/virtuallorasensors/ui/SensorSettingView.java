package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import hu.bme.igno1v.temalabor.virtuallorasensors.R;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorBase;

/**
 * Setting view for a single sensor in SensorSettingsActivity.
 */
public class SensorSettingView extends LinearLayout {

    /**
     * Related sensor.
     */
    private SensorBase sensor;

    /**
     * Initialize the view, inflate from R.layout.sensorsetting.
     * Set the checked state of the checkbox to the used state of the sensor.
     *
     * @param context Context where view is shown
     * @param sensor  Related sensor
     */
    public SensorSettingView(Context context, SensorBase sensor) {

        super(context);
        this.sensor = sensor;

        View view = LayoutInflater.from(context).inflate(R.layout.sensorsetting, this);

        CheckBox useSensorCheckBox = (CheckBox) view.findViewById(R.id.useSensorCheckBox);
        useSensorCheckBox.setOnCheckedChangeListener(new CheckboxChangeListener());
        useSensorCheckBox.setText(sensor.getName());
        useSensorCheckBox.setChecked(sensor.isUsed());

    }

    /**
     * Listener of the checked state change event of the checkbox.
     */
    private class CheckboxChangeListener implements CompoundButton.OnCheckedChangeListener {

        /**
         * Handle the checked state change event: set the used state of the sensor to
         * the checked state of the checkbox.
         *
         * @param buttonView View that's checked state is changed
         * @param isChecked  New checked state of the checkbox
         */
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            sensor.setUsed(isChecked);
        }

    }

}
