package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import hu.bme.igno1v.temalabor.virtuallorasensors.R;
import hu.bme.igno1v.temalabor.virtuallorasensors.Settings;
import hu.bme.igno1v.temalabor.virtuallorasensors.lora.TransmitScheduler;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorList;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Main activity of the application:
 * - showing values of sensors
 * - showing log of events
 */
@RuntimePermissions
public class MainActivity extends AppCompatActivity {

    /**
     * View pager for showing two switchable pages: sensor values, log
     */
    ViewPager mainViewPager;

    /**
     * Key for storing current selected page information in the bundle when activity is destroyed
     */
    private final static String KEY_CURRENT_PAGE = "KEY_CURRENT_PAGE";

    /**
     * Initialize activity, settings and sensors
     *
     * @param savedInstanceState Bundle with saved data if activity is created after being previously destroyed
     */
    @Override
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Settings.init(this);

        SensorList.getInstance().setContext(this);
        SensorList.getInstance().createEverythingByPreset();

        TransmitScheduler.getInstance().setTransmitting(true);

        mainViewPager = (ViewPager) findViewById(R.id.mainViewPager);
        FragmentPagerAdapter mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), this);
        mainViewPager.setAdapter(mainPagerAdapter);

        if (savedInstanceState != null) {
            int mainViewPagerSelectedPage = savedInstanceState.getInt(KEY_CURRENT_PAGE, 0);
            mainViewPager.setCurrentItem(mainViewPagerSelectedPage);
        }

        MainActivityPermissionsDispatcher.requesetPermissionForLocationWithPermissionCheck(this);

    }

    /**
     * Called before activity is destroyed, so we can save state data.
     *
     * @param outState Bundle where all relevant data are saved
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_CURRENT_PAGE, mainViewPager.getCurrentItem());
        super.onSaveInstanceState(outState);
    }

    /**
     * Create the options menu in the right top corner.
     *
     * @param menu Menu where R.menu.menu_main is inflated
     * @return Always true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Handle item select of the options menu.
     *
     * @param item Selected item
     * @return true if method could handle the item selection
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int menuItemId = item.getItemId();
        Intent startActivityIntent;
        boolean newCheckState;

        switch (menuItemId) {
            case R.id.action_settings_virtuallora:
                startActivityIntent = new Intent(this, RadioSettingsActivity.class);
                startActivity(startActivityIntent);
                return true;
            case R.id.action_settings_sensors:
                startActivityIntent = new Intent(this, SensorSettingsActivity.class);
                startActivity(startActivityIntent);
                return true;
            case R.id.action_payload_information:
                startActivityIntent = new Intent(this, PayloadInformationActivity.class);
                startActivity(startActivityIntent);
                return true;
            case R.id.action_pause:
                newCheckState = !item.isChecked();
                item.setChecked(newCheckState);
                SensorList.getInstance().pauseAll(newCheckState);
                return true;
            case R.id.action_transmit:
                newCheckState = !item.isChecked();
                item.setChecked(newCheckState);
                TransmitScheduler.getInstance().setTransmitting(newCheckState);
                return true;
            case R.id.action_about:
                showAbout();
                return true;
            case R.id.action_exit:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    /**
     * Show about dialog.
     */
    private void showAbout() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("About");
        dialogBuilder.setNeutralButton("Close", null);
        dialogBuilder.setView(R.layout.fragment_dialog_about);
        dialogBuilder.show();
    }

    /**
     * Show rationale dialog for location permission request.
     *
     * @param request PermissionRequest object describing the permission request.
     */
    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    public void showRationaleForFineLocation(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_fine_location_rationale)
                .setPositiveButton(R.string.button_proceed, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .show();
    }

    /**
     * Try to request permission for fine location if not given.
     * Called from onCreate().
     */
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void requesetPermissionForLocation() {
    }

    /**
     * Callback for permission request event.
     *
     * @param requestCode  Code of the request
     * @param permissions  Array of requested permissions
     * @param grantResults Array with result of permission requeset: [m] contains 1 if permissions[m] is granted
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Do nothing
    }

}
