package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import hu.bme.igno1v.temalabor.virtuallorasensors.R;
import hu.bme.igno1v.temalabor.virtuallorasensors.lora.PayloadFormatter;
import hu.bme.igno1v.temalabor.virtuallorasensors.lora.TransmitScheduler;
import hu.bme.igno1v.temalabor.virtuallorasensors.sensors.SensorBase;

/**
 * Activity used to show the current format of the FRMPayload.
 */
public class PayloadInformationActivity extends AppCompatActivity {

    /**
     * Table that lists the parts of the payload.
     */
    private TableLayout payloadPartsTable;

    /**
     * Initialize the activity.
     * Create the view by R.layout.activity_payload_information.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payload_information);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listPayloadParts();
    }

    /**
     * Fill the payloadPartsTable with payload information.
     */
    private void listPayloadParts() {

        payloadPartsTable = (TableLayout) findViewById(R.id.payloadPartsTable);
        PayloadFormatter pfInstance = TransmitScheduler.getInstance().getPayloadFormatter();

        int offset = 0;
        int i = 0;
        for (PayloadFormatter.PayloadPart payloadPart : pfInstance.getParts()) {
            inflateRow(payloadPart, i, offset);
            offset += payloadPart.length;
            i++;
        }

        payloadPartsTable.forceLayout();

    }

    /**
     * Add a new row to payloadPartsTable.
     *
     * @param payloadPart Payload part to display in the created row
     * @param rowIndex    Index of the added row (starting with 0)
     * @param offset      Index of the first byte of the payload part displayed by the created row
     */
    private void inflateRow(PayloadFormatter.PayloadPart payloadPart, int rowIndex, int offset) {

        TableRow row = (TableRow) View.inflate(this, R.layout.payloadrow, null);
        if (rowIndex % 2 == 1)
            row.setBackgroundResource(R.drawable.bg_lightgray);

        TextView sensorName = (TextView) row.findViewById(R.id.sensorName);
        TextView valueName = (TextView) row.findViewById(R.id.valueName);
        TextView bytesForWholePart = (TextView) row.findViewById(R.id.bytesForWholePart);
        TextView byteCount = (TextView) row.findViewById(R.id.byteCount);
        TextView offsetStart = (TextView) row.findViewById(R.id.offsetStart);
        TextView offsetEnd = (TextView) row.findViewById(R.id.offsetEnd);

        SensorBase sensor = payloadPart.sensor;
        sensorName.setText(sensor.getName());
        valueName.setText(sensor.getFieldName(payloadPart.valueIndex));
        bytesForWholePart.setText(Integer.toString(sensor.getBytesForWholePart()));
        int length = payloadPart.length;
        byteCount.setText(Integer.toString(length));
        offsetStart.setText(Integer.toString(offset));
        offsetEnd.setText(Integer.toString(offset + length - 1));

        payloadPartsTable.addView(row);

    }

    /**
     * Handle click event of the "back" button in the toolbar
     *
     * @param item Clicked item
     * @return true if succeeded to handle menu item selection
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
