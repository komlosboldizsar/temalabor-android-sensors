package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public abstract class PhysicalSensor extends SensorBase implements SensorEventListener {

    private SensorManager sensorManager;
    private android.hardware.Sensor hardware;

    private final List<SensorListener> listeners = new LinkedList<>();

    public PhysicalSensor(SensorManager sensorManager, final String name, int type) {
        super(name);
        this.sensorManager = sensorManager;
        getHardware(type);
    }

    /* Hardware handling */

    private void getHardware(int type) {
        hardware = sensorManager.getDefaultSensor(type);
        available = (hardware != null);
    }

    private void startHardwareListening() {
        sensorManager.registerListener(this, hardware, SensorManager.SENSOR_DELAY_NORMAL);
        notificateListeners();
    }

    private void stopHardwareListening() {
        sensorManager.unregisterListener(this);
        notificateListeners();
    }

    private void setHardwareListening() {
        if (used && !paused)
            startHardwareListening();
        else
            stopHardwareListening();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        this.currentValues = event.values;
        this.lastUpdate = new Date();
        notificateListeners();
    }

    @Override
    public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {
        // Do nothing
    }

    /* ... */

    public void setUsed(boolean isUsed) {
        super.setUsed(isUsed);
        setHardwareListening();
    }

    public void setPaused(boolean isPaused) {
        super.setPaused(isPaused);
        setHardwareListening();
    }

}
