package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.hardware.SensorManager;

public class Proximity extends PhysicalSensor {

    public static final int VALUE_DISTANCE = 0;

    public Proximity(SensorManager sensorManager) {
        super(sensorManager, "Proximity", android.hardware.Sensor.TYPE_PROXIMITY);
        addField(VALUE_DISTANCE, "Distance", "cm");
        setBytesRequiredForWholePart(1);
    }

}
