package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import hu.bme.igno1v.temalabor.virtuallorasensors.Logger;
import hu.bme.igno1v.temalabor.virtuallorasensors.R;

/**
 * View for a log message in the MainLogFragment.
 */
public class LogMessageView extends LinearLayout {

    /**
     * Data format used to show timestamp of events.
     */
    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss.S");

    /**
     * Color used for neural log messages.
     */
    private final static int MESSAGECOLOR_NEUTRAL = Color.BLACK;

    /**
     * Color used for error messages.
     */
    private final static int MESSAGECOLOR_ERROR = Color.argb(100, 160, 0, 0);

    /**
     * Color used for success messages.
     */
    private final static int MESSAGECOLOR_SUCCESS = Color.argb(100, 0, 140, 0);

    /**
     * Color used for warning messages.
     */
    private final static int MESSAGECOLOR_WARNING = Color.argb(100, 255, 180, 0);

    /**
     * Initialize the view and fill fields.
     *
     * @param context   Context where view is used
     * @param message   Message to show
     * @param type      Type of log message
     * @param timestamp Timestamp of the event
     */
    public LogMessageView(Context context, String message, Logger.MessageType type, Date timestamp) {

        super(context);
        LayoutInflater.from(context).inflate(R.layout.logmessage, this);

        String messageStr = "[" + DATE_FORMAT.format(timestamp) + "] " + message;
        TextView messageTextView = (TextView) findViewById(R.id.messageText);
        messageTextView.setText(Html.fromHtml(messageStr));

        switch (type) {
            case LOG_NEUTRAL:
                messageTextView.setTextColor(MESSAGECOLOR_NEUTRAL);
                break;
            case LOG_ERROR:
                messageTextView.setTextColor(MESSAGECOLOR_ERROR);
                break;
            case LOG_SUCCESS:
                messageTextView.setTextColor(MESSAGECOLOR_SUCCESS);
                break;
            case LOG_WARNING:
                messageTextView.setTextColor(MESSAGECOLOR_WARNING);
                break;
        }

    }

}
