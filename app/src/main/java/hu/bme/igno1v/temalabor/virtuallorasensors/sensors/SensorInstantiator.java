package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.app.Service;
import android.content.Context;
import android.hardware.SensorManager;
import android.location.LocationManager;

public class SensorInstantiator {

    public enum SensorType {
        ACCELEROMETER,
        LINEAR_ACCELERATION,
        GRAVITY,
        AMBIENT_LIGHT,
        AMBIENT_TEMPERATURE,
        GYROSCOPE,
        MAGNETIC_FIELD,
        PROXIMITY,
        ROTATION_VECTOR,
        LOCATION_GPS,
        LOCATION_NETWORK,
        BATTERY
    }

    private final Context context;
    private final SensorManager sensorManager;
    private final LocationManager locationManager;

    public SensorInstantiator(Context context) {
        this.context = context;
        this.sensorManager = (SensorManager) context.getSystemService(Service.SENSOR_SERVICE);
        this.locationManager = (LocationManager) context.getSystemService(Service.LOCATION_SERVICE);
    }

    public SensorBase createInstance(SensorType type) {
        switch (type) {
            case ACCELEROMETER:
                return new Accelerometer(sensorManager);
            case LINEAR_ACCELERATION:
                return new LinearAcceleration(sensorManager);
            case GRAVITY:
                return new Gravity(sensorManager);
            case AMBIENT_LIGHT:
                return new AmbientLight(sensorManager);
            case AMBIENT_TEMPERATURE:
                return new AmbientTemperature(sensorManager);
            case GYROSCOPE:
                return new Gyroscope(sensorManager);
            case MAGNETIC_FIELD:
                return new MagneticField(sensorManager);
            case PROXIMITY:
                return new Proximity(sensorManager);
            case ROTATION_VECTOR:
                return new RotationVector(sensorManager);
            case LOCATION_GPS:
                return new Location(locationManager, LocationManager.GPS_PROVIDER, "Location (GPS)");
            case LOCATION_NETWORK:
                return new Location(locationManager, LocationManager.NETWORK_PROVIDER, "Location (network)");
            case BATTERY:
                return new Battery(context, "Battery");
        }
        return null;
    }

}
