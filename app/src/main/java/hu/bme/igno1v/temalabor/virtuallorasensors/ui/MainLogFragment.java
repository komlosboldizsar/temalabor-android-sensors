package hu.bme.igno1v.temalabor.virtuallorasensors.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ScrollView;

import java.util.Date;

import hu.bme.igno1v.temalabor.virtuallorasensors.Logger;
import hu.bme.igno1v.temalabor.virtuallorasensors.R;

/**
 * Fragment for MainActivity that shows log messages.
 */
public class MainLogFragment extends android.support.v4.app.Fragment implements Logger.LoggerListener {

    /**
     * Context where fragment is used.
     */
    private Context context;

    /**
     * Scrollable view for log messages.
     */
    private ScrollView scrollView;

    /**
     * View group where log messages are put.
     */
    private ViewGroup contentViewGroup;

    /**
     * True if scrollView is always scrolled to bottom when a new log message is added.
     */
    private boolean autoscroll = true;

    /**
     * True if contentViewGroup is auto cleared so it doesn't contain million of items.
     */
    private boolean autoclear = true;

    /**
     * Minimum quantity of log messages to use the auto clear function.
     */
    private static final int AUTOCLEAR_MINIMUM_QUANTITY = 100;

    /**
     * How many messages must remain in contentViewGroup when some of them are deleted with the auto clear function.
     */
    private static final int AUTOCLEAR_REMAINING_QUANTITY = 50;

    /**
     * Initialize the view, bind elements (checkboxes and button), register Logger listener.
     *
     * @param inflater           Inflater to inflate the layout XML
     * @param container          Root/parent element of the view
     * @param savedInstanceState Not used
     * @return The created (inflated view)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        context = getContext();
        View view = inflater.inflate(R.layout.fragment_main_log, container, false);

        scrollView = (ScrollView) view.findViewById(R.id.logScroller);
        contentViewGroup = (ViewGroup) view.findViewById(R.id.logContent);

        Button clearButton = (Button) view.findViewById(R.id.clearLogButton);
        clearButton.setOnClickListener(new ClearButtonListener());

        CheckBox autoScrollCheckbox = (CheckBox) view.findViewById(R.id.autoScrollCheckbox);
        autoScrollCheckbox.setOnCheckedChangeListener(new AutoScrollCheckboxListener());

        CheckBox autoClearCheckbox = (CheckBox) view.findViewById(R.id.autoClearCheckbox);
        autoClearCheckbox.setOnCheckedChangeListener(new AutoClearCheckboxListener());

        Logger.registerListener(this);

        return view;

    }

    /**
     * When view is destroyed, unsubscribe from Logger.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Logger.unregisterListener(this);
    }

    /**
     * New log message arrives: Create LogMessageView object and show it in the contentViewGroup.
     * Use autoclear function if necessary.
     *
     * @param message   Message to show
     * @param type      Type of message
     * @param timestamp Timestamp of the event related to the log message
     */
    @Override
    public void newLogMessage(final String message, final Logger.MessageType type, final Date timestamp) {

        // Must do on UI thread
        Runnable displayMessageRunnable = new Runnable() {
            @Override
            public void run() {

                LogMessageView messageView = new LogMessageView(context, message, type, timestamp);
                contentViewGroup.addView(messageView);

                if (autoclear && (contentViewGroup.getChildCount() > AUTOCLEAR_MINIMUM_QUANTITY)) {
                    int count = contentViewGroup.getChildCount() - AUTOCLEAR_REMAINING_QUANTITY;
                    contentViewGroup.removeViews(0, count);
                }

                if (autoscroll)
                    scrollView.fullScroll(View.FOCUS_DOWN);

            }
        };

        getActivity().runOnUiThread(displayMessageRunnable);

    }

    /**
     * Listener of the clear button.
     */
    private class ClearButtonListener implements View.OnClickListener {
        /**
         * Clear the contentViewGroup view (delete all log messages).
         *
         * @param v View that is clicked (hopefully the clear button)
         */
        @Override
        public void onClick(View v) {
            contentViewGroup.removeAllViews();
        }
    }

    /**
     * Check state change listener for the autoscroll checkbox.
     */
    private class AutoScrollCheckboxListener implements CompoundButton.OnCheckedChangeListener {
        /**
         * Set the autoscroll property by the check state of the autoscroll checkbox.
         *
         * @param buttonView View that's check state changed (hopefully the autoscroll checkbox)
         * @param isChecked  The new check state of the changed element
         */
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            autoscroll = isChecked;
        }
    }

    /**
     * Check state change listener for the autoclear checkbox.
     */
    private class AutoClearCheckboxListener implements CompoundButton.OnCheckedChangeListener {
        /**
         * Set the autoclear property by the check state of the autoclear checkbox.
         *
         * @param buttonView View that's check state changed (hopefully the autoclear checkbox)
         * @param isChecked  The new check state of the changed element
         */
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            autoclear = isChecked;
        }
    }

}
