package hu.bme.igno1v.temalabor.virtuallorasensors;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Log message dispatcher.
 */
public class Logger {

    /**
     * Listeners who observe new log messages.
     */
    private final static List<LoggerListener> listeners = new LinkedList<>();

    /**
     * Create a new log message with type LOG_NEUTRAL.
     *
     * @param message Text of the message
     */
    public static void log(String message) {
        log(message, MessageType.LOG_NEUTRAL);
    }

    /**
     * Create a new log message with given type.
     *
     * @param message Text of the message
     * @param type    Type of the messagex
     */
    public static void log(String message, MessageType type) {
        Date timestamp = new Date();
        for (LoggerListener listener : listeners)
            listener.newLogMessage(message, type, timestamp);
    }

    /**
     * Register a new observer for new log messages.
     *
     * @param listener Listener to subscribe
     */
    public static void registerListener(LoggerListener listener) {
        listeners.add(listener);
    }

    /**
     * Unregister an observer for new log messages.
     *
     * @param listener Listener to unsubscribe
     */
    public static void unregisterListener(LoggerListener listener) {
        listeners.remove(listener);
    }

    /**
     * Interface of classes that observe new log messages.
     */
    public interface LoggerListener {

        /**
         * New log message is received.
         *
         * @param message   Text of message
         * @param type      Type of message
         * @param timestamp Timestamp of related event
         */
        void newLogMessage(String message, MessageType type, Date timestamp);

    }

    /**
     * Possible type of log messages
     */
    public enum MessageType {
        LOG_SUCCESS,
        LOG_ERROR,
        LOG_WARNING,
        LOG_NEUTRAL
    }

}
