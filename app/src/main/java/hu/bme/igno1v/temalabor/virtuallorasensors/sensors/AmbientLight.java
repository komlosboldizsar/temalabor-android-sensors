package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.hardware.SensorManager;

public class AmbientLight extends PhysicalSensor {

    public static final int VALUE_LIGHT = 0;

    public AmbientLight(SensorManager sensorManager) {
        super(sensorManager, "Ambient light", android.hardware.Sensor.TYPE_LIGHT);
        addField(VALUE_LIGHT, "Lightness", "lx");
        setBytesRequiredForWholePart(1);
    }

}
