package hu.bme.igno1v.temalabor.virtuallorasensors;

import android.content.Context;
import android.content.SharedPreferences;

import hu.bme.igno1v.temalabor.virtuallorasensors.lora.RadioInterface;
import hu.bme.igno1v.temalabor.virtuallorasensors.lora.TransmitScheduler;

public class Settings {

    private static SharedPreferences sharedPreferences;

    private static final String SETTINGS_NAME = "VIRTUAL_LORA_SETTINGS";

    private static final String KEY_DEVICE_ID = "DEVICE_ID";
    private static final String KEY_LORA_SOCKET_IP = "LORA_SOCKET_IP";
    private static final String KEY_LORA_SOCKET_PORT = "LORA_SOCKET_PORT";
    private static final String KEY_TRANSMIT_RATE = "KEY_TRANSMIT_RATE";

    private static final String DEFAULT_DEVICE_ID = "00:00:00:00:00:00:00:00";
    private static final String DEFAULT_LORA_SOCKET_IP = "127.0.0.1";
    private static final int DEFAULT_LORA_SOCKET_PORT = 20400;
    private static final int DEFAULT_TRANSMIT_RATE = 5000;

    private static final String REGEXP_DEVICE_ID = "^([0-9a-fA-F]{2}:){7}([0-9a-fA-F]{2})$";

    public static void init(Context context) {
        Settings.sharedPreferences = context.getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        loadAllSettings();
    }

    /* Set settings individually */

    public static void setDeviceID(String deviceID) {
        if (!deviceID.matches(REGEXP_DEVICE_ID))
            return;
        RadioInterface.getInstance().setDeviceID(parseDeviceId(deviceID));
        saveStringSetting(KEY_DEVICE_ID, deviceID);
    }

    public static void setLoraSocketIP(String ipAddress) {
        RadioInterface.getInstance().setSocketIP(ipAddress);
        saveStringSetting(KEY_LORA_SOCKET_IP, ipAddress);
    }

    public static void setLoraSocketPort(int portNumber) {
        RadioInterface.getInstance().setSocketPortNumber(portNumber);
        saveIntSetting(KEY_LORA_SOCKET_PORT, portNumber);
    }

    public static void setTransmissionRate(int milliseconds) {
        TransmitScheduler.getInstance().setTransmitRate(milliseconds);
        saveIntSetting(KEY_TRANSMIT_RATE, milliseconds);
    }

    /* Get setting values individually */

    public static String getDeviceID() {
        return loadStringSetting(KEY_DEVICE_ID, DEFAULT_DEVICE_ID);
    }

    public static String getLoraSocketIP() {
        return loadStringSetting(KEY_LORA_SOCKET_IP, DEFAULT_LORA_SOCKET_IP);
    }

    public static int getLoraSocketPort() {
        return loadIntSetting(KEY_LORA_SOCKET_PORT, DEFAULT_LORA_SOCKET_PORT);
    }

    public static int getTransmissionRate() {
        return loadIntSetting(KEY_TRANSMIT_RATE, DEFAULT_TRANSMIT_RATE);
    }

    /* Load all settings */

    private static void loadAllSettings() {
        RadioInterface.getInstance().setDeviceID(parseDeviceId(getDeviceID()));
        RadioInterface.getInstance().setSocketIP(getLoraSocketIP());
        RadioInterface.getInstance().setSocketPortNumber(getLoraSocketPort());
        TransmitScheduler.getInstance().setTransmitRate(getTransmissionRate());
    }

    /* Put value in shared preferences */

    private static void saveStringSetting(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private static void saveIntSetting(String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /* Get value from shared preferences */

    private static int loadIntSetting(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    private static String loadStringSetting(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    /* Helpers, converters */

    private static byte[] parseDeviceId(String deviceID) {
        byte[] output = new byte[8];
        String[] parts = deviceID.split(":");
        for (int i = 0; i < 8; i++)
            output[i] = Byte.parseByte(parts[i], 16);
        return output;
    }

}
