package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.hardware.SensorManager;

public class AmbientTemperature extends PhysicalSensor {

    public final int VALUE_TEMPERATURE = 0;

    public AmbientTemperature(SensorManager sensorManager) {
        super(sensorManager, "Ambient temperature", android.hardware.Sensor.TYPE_AMBIENT_TEMPERATURE);
        addField(VALUE_TEMPERATURE, "Temperature", "°C");
        setBytesRequiredForWholePart(1);
        setValuesCanBeSigned(true);
    }

}
