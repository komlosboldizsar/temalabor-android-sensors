package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.hardware.SensorManager;

public class MagneticField extends PhysicalSensor {

    public final int VALUE_X = 0;
    public final int VALUE_Y = 1;
    public final int VALUE_Z = 2;

    public MagneticField(SensorManager sensorManager) {
        super(sensorManager, "Magnetic field", android.hardware.Sensor.TYPE_MAGNETIC_FIELD);
        addField(VALUE_X, "Axis X", "μT");
        addField(VALUE_Y, "Axis Y", "μT");
        addField(VALUE_Z, "Axis Z", "μT");
        setBytesRequiredForWholePart(1);
        setValuesCanBeSigned(true);
    }

}
