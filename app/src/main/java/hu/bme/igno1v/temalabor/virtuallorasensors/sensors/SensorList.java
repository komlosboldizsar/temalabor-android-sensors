package hu.bme.igno1v.temalabor.virtuallorasensors.sensors;

import android.content.Context;
import android.hardware.SensorManager;
import android.location.LocationManager;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import hu.bme.igno1v.temalabor.virtuallorasensors.lora.PayloadFormatter;
import hu.bme.igno1v.temalabor.virtuallorasensors.lora.TransmitScheduler;

public class SensorList {

    private Map<String, SensorBase> sensors;
    private Context context = null;

    private static SensorList instance = null;

    private final PresetSensor[] PRESET_SENSORS = new PresetSensor[]{
            new PresetSensor(SensorInstantiator.SensorType.ACCELEROMETER, "ACCELEROMETER_0"),
            new PresetSensor(SensorInstantiator.SensorType.LINEAR_ACCELERATION, "LINEAR_ACCELERATION_0"),
            new PresetSensor(SensorInstantiator.SensorType.GRAVITY, "GRAVITY_0"),
            new PresetSensor(SensorInstantiator.SensorType.AMBIENT_LIGHT, "AMBIENT_LIGHT_0"),
            new PresetSensor(SensorInstantiator.SensorType.AMBIENT_TEMPERATURE, "AMBIENT_TEMPERATURE_0"),
            new PresetSensor(SensorInstantiator.SensorType.GYROSCOPE, "GYROSCOPE_0"),
            new PresetSensor(SensorInstantiator.SensorType.PROXIMITY, "PROXIMITY_0"),
            new PresetSensor(SensorInstantiator.SensorType.MAGNETIC_FIELD, "MAGNETIC_FIELD_0"),
            new PresetSensor(SensorInstantiator.SensorType.ROTATION_VECTOR, "ROTATION_VECTOR_0"),
            new PresetSensor(SensorInstantiator.SensorType.LOCATION_GPS, "LOCATION_GPS_0"),
            new PresetSensor(SensorInstantiator.SensorType.LOCATION_NETWORK, "LOCATION_NETWORK_0"),
            new PresetSensor(SensorInstantiator.SensorType.BATTERY, "BATTERY_0"),
    };

    private final PresetPayloadPart[] PRESET_PAYLOAD_PARTS = new PresetPayloadPart[]{
            new PresetPayloadPart("ACCELEROMETER_0", Accelerometer.VALUE_X, 2),
            new PresetPayloadPart("ACCELEROMETER_0", Accelerometer.VALUE_Y, 2),
            new PresetPayloadPart("ACCELEROMETER_0", Accelerometer.VALUE_Z, 2),
            new PresetPayloadPart("AMBIENT_LIGHT_0", AmbientLight.VALUE_LIGHT, 1),
            new PresetPayloadPart("PROXIMITY_0", Proximity.VALUE_DISTANCE, 1)
    };

    private SensorList() {
        sensors = new TreeMap<>();
    }

    public static SensorList getInstance() {
        if (instance == null)
            instance = new SensorList();
        return instance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void addSensor(SensorBase sensor, String ref) {
        sensors.put(ref, sensor);
    }

    public void createEverythingByPreset() {
        createSensorsByPreset();
        setPayloadFormatByPreset();
    }

    private void createSensorsByPreset() {
        SensorInstantiator instantiator = new SensorInstantiator(context);
        for (PresetSensor ps : PRESET_SENSORS) {
            SensorBase sensor = instantiator.createInstance(ps.type);
            addSensor(sensor, ps.ref);
        }
    }

    private void setPayloadFormatByPreset() {

        PayloadFormatter payloadFormatter = new PayloadFormatter();

        for (PresetPayloadPart ppp : PRESET_PAYLOAD_PARTS) {
            SensorBase sensor = sensors.get(ppp.ref);
            if (sensor != null)
                payloadFormatter.addPart(sensor, ppp.valueIndex, ppp.valueLength);
        }

        TransmitScheduler.getInstance().setPayloadFormatter(payloadFormatter);

    }

    public List<SensorBase> getSensorsList() {
        Collection<SensorBase> sensorCollection = sensors.values();
        List<SensorBase> returnList = new LinkedList<>(sensorCollection);
        return returnList;
    }

    public void pauseAll(boolean arePaused) {
        for (SensorBase s : sensors.values())
            s.setPaused(arePaused);
    }

    private void sensorListChanged() {
        notificateChangeListeners();
    }

    private List<SensorListChangeListener> changeListeners = new LinkedList<>();

    public void registerSensorListChangeListener(SensorListChangeListener listener) {
        changeListeners.add(listener);
    }

    private void notificateChangeListeners() {
        for (SensorListChangeListener listener : changeListeners)
            listener.sensorListChanged();
    }

    public interface SensorListChangeListener {
        void sensorListChanged();
    }

    private class PresetSensor {
        public final SensorInstantiator.SensorType type;
        public final String ref;

        public PresetSensor(SensorInstantiator.SensorType type, String ref) {
            this.type = type;
            this.ref = ref;
        }
    }

    private class PresetPayloadPart {
        public final String ref;
        public final int valueIndex;
        public final int valueLength;

        public PresetPayloadPart(String ref, int valueIndex, int valueLength) {
            this.ref = ref;
            this.valueIndex = valueIndex;
            this.valueLength = valueLength;
        }
    }

}
